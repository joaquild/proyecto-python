import os
import subprocess
from subprocess import PIPE
from frontend.complemento import whoami, desactivar_botones_si_nada_seleccionado
from backend.actualizar import update

def command_git_push(tabla_proyectos, btn_1, btn_2, btn_3, btn_4, usuario_in, contraseña_in, commit_in):
    usuario_gitlab = usuario_in.get()
    contraseña_gitlab = contraseña_in.get()
    commit_gitlab = commit_in.get()

    if commit_gitlab == "":
        commit_gitlab = "estandar commit"

    print(usuario_gitlab)
    print(commit_gitlab)

    seleccionado = tabla_proyectos.selection()	
    item = [tabla_proyectos.item(seleccionado)["text"]]
    updated_item = str(item)[2:-2]

    print ("updated item: " + updated_item)

    user = whoami()
    directorio_base = os.path.join("/home", user, "Proyectos")
    dir_repositorio = os.path.join(directorio_base, updated_item, "scripts")
    
    print(dir_repositorio)

    argumento1 = "push"
    print(argumento1)
    argumento2 = "-u"
    print(argumento2)
    argumento3 = "https://gitlab.com/" + usuario_gitlab + "/" + updated_item + ".git"
    print(argumento3)
    argumento4 = "master"
    print(argumento4)
    argumentos = " ".join([argumento1, argumento2, argumento3, argumento4])
    print(argumentos)

    init = subprocess.Popen(["git", "init" ], cwd = "{dir_repositorio2}".format(dir_repositorio2 = dir_repositorio))
    add = subprocess.Popen(["git", "add", "."], cwd = "{dir_repositorio2}".format(dir_repositorio2 = dir_repositorio))
    commit = subprocess.Popen(["git", "commit", "-m \"{commit_gitlab2}\"".format(commit_gitlab2 = commit_gitlab)], cwd = "{dir_repositorio2}".format(dir_repositorio2 = dir_repositorio))
    push = subprocess.Popen(["git", "push", "-u", argumento3, "master"], cwd = "{dir_repositorio2}".format(dir_repositorio2= dir_repositorio), stdin=PIPE)
    input_string = "{usuario}\n{contraseña}\n".format(usuario = usuario_gitlab, contraseña = contraseña_gitlab)
    push_input = push.communicate(input = input_string.encode("utf-8"))[0]
    update(tabla_proyectos)