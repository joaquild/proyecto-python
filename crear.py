from tkinter import *
import os
import subprocess
import getpass
from subprocess import Popen, PIPE, STDOUT
from frontend.complemento import whoami, desactivar_botones_si_nada_seleccionado
from backend.actualizar import update
from frontend.error_crear import crear_error
from backend.crear_repositorio import iniciar_repositorio
from backend.funcion_crear_A import A
from backend.funcion_crear_B import B

def crear_proyecto(tabla_proyectos, btn_1, btn_2, btn_3, btn_4):
	new_window = Toplevel()
	new_window.geometry("500x400")
	new_window.configure(bg="#D1D6D6")
	lbl10 = Label(new_window, text="¿Tipo de proyecto?", font=("Arial", 20), bg="#D1D6D6", fg="black")
	new_window.minsize(width=500, height=400)
	new_window.maxsize(width=500, height=400)

	lbl10.place(x=20, y=20)

	opciones = ttk.Combobox(new_window,
		        state="readonly")
	opciones["values"] = ["Proyecto A", "Proyecto B"]

	opciones.set("Seleccionar")
	opciones.place(x=20, y=80)

	lbl33 = Label(new_window, text="Según el tipo de proyecto que selecciones, A o B, aqui se mostrará su correspondiente definición y contenido.", bg="white", fg="black", font=("Arial", 10), wraplength=137, justify="left")
    	
	lbl33.place(x=310, y=80, height=150)
    	
	def modified(event):
		
		if (event.widget.get() == "Proyecto A"):
			lbl33.config(text="Al seleccionar el Proyecto A, va a obtener las siguientes carpetas: Docs, Scripts, Inputs, Outputs")
				
			btn32.config(command=lambda:(A(tabla_proyectos, btn_1, btn_2, btn_3, btn_4)))
		    
		if (event.widget.get() == "Proyecto B"):
			lbl33.config(text="Al seleccionar el Proyecto B, va a obtener las siguientes carpetas: HTML, CSS, JavaScript")		

			btn32.config(command=lambda:(B(tabla_proyectos, btn_1, btn_2, btn_3, btn_4)))
            
	opciones.bind('<<ComboboxSelected>>', modified)

	btn32 = Button(new_window,
	   text="Siguiente",
	   bg="black",
	   fg="white",
	   font=("Arial", 10))
	   
	btn32.place(x=410,
	    y=350)
	    