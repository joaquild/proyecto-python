from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from frontend.complemento import whoami, desactivar_botones_si_nada_seleccionado
from backend.actualizar import update
from frontend.error_crear import crear_error
from backend.crear_repositorio import iniciar_repositorio

def A(tabla_proyectos, btn_1, btn_2, btn_3, btn_4):
    password = StringVar()	
    ID = StringVar()
    usuario_in = StringVar()
    passwordlab_in = StringVar()

    def ejecutar():
        user = whoami()
        ID_proyecto = ID.get()
        usuario_gitlab = usuario_in.get()
        contraseña_gitlab = passwordlab_in.get()

        print (ID_proyecto)
        directorio_base = os.path.join("/home", user, "Proyectos")
        print (directorio_base)
        subprocess.call(["mkdir", directorio_base])
        dir_desencriptado = os.path.join(directorio_base, ID_proyecto)
        print (dir_desencriptado)
        dir_encriptado = os.path.join (directorio_base, "." + ID_proyecto + "_enc")
        print (dir_encriptado)
        carpeta_1 = os.path.join(dir_desencriptado, "output")
        carpeta_2 = os.path.join(dir_desencriptado, "input")
        carpeta_3 = os.path.join(dir_desencriptado, "docs")
        carpeta_4 = os.path.join(dir_desencriptado, "scripts")
        passwd = password.get()

        zymvol = subprocess.Popen(["encfs", "-S", dir_encriptado, dir_desencriptado], stdout=PIPE, stdin=PIPE)
        input_string = "y\ny\n{passwd}\n{passwd}\n".format(passwd = password.get())
        print (input_string)
        prueba24 = zymvol.communicate(input= input_string.encode("utf-8"))[0]
        prueba24
        subprocess.Popen(["mkdir", carpeta_1, carpeta_2, carpeta_3, carpeta_4])
        proyecto_a.destroy()

        tabla_proyectos.insert("",0, text=ID_proyecto, values=(dir_desencriptado, "Montado"))
        crear_error(tabla_proyectos, ID_proyecto, dir_desencriptado)
        iniciar_repositorio(tabla_proyectos, usuario_gitlab, contraseña_gitlab, ID_proyecto, dir_desencriptado)
        desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3, btn_4)

    proyecto_a = Toplevel()
    proyecto_a.geometry("500x500")
    proyecto_a.configure(bg="#D1D6D6")
    proyecto_a.title("Proyecto A")
    proyecto_a.minsize(width=500, height=500)
    proyecto_a.maxsize(width=500, height=500)

    lbl = Label(proyecto_a, text="Configuración", bg="#D1D6D6", fg="black", font=("Arial", 18))
    lbl.place(x=75, y= 25)

    lbl2 = Label(proyecto_a, text="Nombre del proyecto:", bg="#D1D6D6", fg="black", font=("Arial", 12))
    lbl2.place(x=75, y= 80)

    lbl1 = Label(proyecto_a, text="Contraseña:", bg="#D1D6D6", fg="black", font=("Arial", 12))
    lbl1.place(x=75, y=150)

    lbl3 = Label(proyecto_a, text="Introduze el usuario de gitlab:", bg="#D1D6D6", fg="black", font=("Arial", 12))
    lbl3.place(x=75, y=220)

    lbl4 = Label(proyecto_a, text="Introduze la contraseña de gitlab:", bg="#D1D6D6", fg="black", font=("Arial", 12))
    lbl4.place(x=75, y=290)

    nombre_proyecto = Entry (proyecto_a, fg="black", font="Arial", textvariable=ID)
    nombre_proyecto.place(x=75, y=105)

    contraseña_entry = Entry(proyecto_a, fg="black", font="Arial", textvariable=password)
    contraseña_entry.place(x=75, y= 175)

    usuario_entry = Entry(proyecto_a, fg="black", font="Arial", textvariable=usuario_in)
    usuario_entry.place(x=75, y= 245)

    contraseña1_entry = Entry(proyecto_a, show="*", fg="black", font="Arial", textvariable=passwordlab_in)
    contraseña1_entry.place(x=75, y=315)

    btn = Button(proyecto_a, text="Realizar", bg="black", fg="white", command = ejecutar)
    btn.place(x=400, y= 450)